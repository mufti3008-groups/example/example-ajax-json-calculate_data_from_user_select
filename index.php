<!DOCTYPE html>
<html>
<head>
	<title>Example Ajax get JSON data</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<div>

<form>
	<h4>registration</h4>
	<!-- option from this select is retrieved from json data -->
	<select id="regType">
		<option value="" selected="" disabled="">--Choose registration type--</option>
	</select>

	<h4>additional paper</h4>
	<!-- option from this select is retrieved from json data -->
	<select id="addPaper" disabled="">
		<option value="0" selected="" disabled="">None</option>
	</select>
	<div>
		<h2><strong>Total</strong> : Rp.<span id="displayResult"></span></h2>
	</div>
	<div>
		<button id="check" type="button">check</button>
		<button id="result" type="button">result</button>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function(){
		var regTypeData=addPaperData=null;
		var	harga_regType=harga_addpaper=total=0;
		$.getJSON( "regType.json", function( data ) {
      // console.log(data); // to print data retrieved from json file, uncoment this
      // write option in tag "select" which id "regType"
      $.each(data,function(key,val){
      	$("#regType").append("<option value=\""+val.id_regType+"\">"+val.text+"</option>")
      });

      // store json data in variable
      regTypeData = data;
		})
		// if failed to load the file regType.json
		.fail(function(xhr, ajaxOptions, thrownError){
			console.log("load regType.json failed...");
      // console.log(xhr);
      // console.log(thrownError);
		});

		$.getJSON( "additionalPaper.json", function( data ) {
      // console.log(data); // to print data retrieved from json file, uncoment this
      // write option in tag "select" which id "regType"
      $.each(data,function(key,val){
      	$("#addPaper").append("<option value=\""+val.id_addpaper+"\">"+val.text+"</option>")
      });

      // store json data in variable
      addPaperData = data;

		})
		// if failed to load the file regType.json
		.fail(function(xhr, ajaxOptions, thrownError){
			console.log("load additionalPaper.json failed...");
      // console.log(xhr);
      // console.log(thrownError);
		});

		// on change select which id "regType" 
		$("#regType").on("change",function(){
			// catch the value
			var id_regType = $(this).val();
			// catch the "harga" which macthing the id_regType from retrieved json with selected value
			$.each(regTypeData,function(key,val){
				if (val.id_regType==id_regType) {
					// console.log(val.harga);
					harga_regType = val.harga;

					// to change "disabled" atribute from element "select" with id "addPaper"
					if (val.paper) {
						// remove attribute disabled
						$("#addPaper").removeAttr("disabled");
					}else{
						// add attribute disabled
						harga_addpaper = 0; // reset "harga additional paper"
						$("#addPaper").attr("disabled","true").find("option").first().removeAttr("selected").attr("selected","");
					}
					return false; // stop the looping
				}
			});
			// counting the total from harga_addpaper and harga_regType
			total=parseInt(harga_addpaper)+parseInt(harga_regType);
		});

		// on change select which id "addPaper" 
		$("#addPaper").on("change",function(){
			// catch the value
			var id_addpaper = $(this).val();
			// catch the "harga" which macthing the id_addpaper from retrieved json with selected value
			$.each(addPaperData,function(key,val){
				if (val.id_addpaper==id_addpaper) {
					// console.log(val.harga);
					harga_addpaper = val.harga;
					return false; // stop the looping
				}
			});
			// counting the total from harga_addpaper and harga_regType
			total=parseInt(harga_addpaper)+parseInt(harga_regType);
		});

		$("#check").click(function(){
			console.log("regTypeData = "+JSON.stringify(regTypeData));
			console.log("addPaperData = "+JSON.stringify(addPaperData));
			console.log("harga_regType = "+harga_regType);
			console.log("harga_addpaper = "+harga_addpaper);
			console.log("total = "+total);
		});
		$("#result").click(function(){
			// displaying the total
			$("#displayResult").html(total);
		});
	});
</script>
</div>
</body>
</html>
